---

getmyip:
  scheduling_group: backend
  num_instances: 1
  monitoring_endpoints:
    - job_name: getmyip
      port: 9002
      scheme: http
  containers:
    - name: getmyip
      image: registry.0xacab.org/leap/getmyip:latest
      port: 9001
      volumes:
        - /var/lib/GeoIP: /var/lib/GeoIP/
  public_endpoints:
    - name: getmyip
      port: 9001
      scheme: http

openvpn:
  # these are udp/tcp services, so we don't want to put them behind the reverse proxy
  # Options:
  #   1) set scheduling_group=frontend,
  #   2) create a 'vpn' group, intended for horizontal scaling of the public vpn nodes,
  #      and run this service there (so you can keep assuming that 'backend' hosts are 'private')
  #
  # we want to schedule the openvpn-frontend role on the 'frontend' groups,
  # there is no openvpn-frontend service, but instead we are customizing the
  # 'frontend' role
  #
  # We do not want to use public_tcp_endpoints for openvpn/shapeshifter, because
  # that results in haproxy being setup for the port. Reverse proxies are
  # connection based, and we want to also be able to do UDP, so we do not really
  # want to have a reverse proxy for openvpn, instead we want to set it up as a
  # user-facing service, and just rely on DNS for request routing and balancing.
  scheduling_group: frontend
  num_instances: 1
  monitoring_endpoints:
    - job_name: openvpn
      port: 9176
      scheme: http
    - job_name: knot-resolver
      port: 8453
      scheme: http
  containers:
    - name: openvpn
      image: registry.0xacab.org/leap/container-platform/openvpn:latest
      ports: [1194, 23042]
      drop_capabilities: false
      docker_options: '--cap-add=NET_ADMIN --cap-add=CAP_NET_BIND_SERVICE'
      volumes:
        - /etc/leap: /etc/leap
        - /etc/credentials/sspki: /etc/credentials/sspki
        - /srv/leap/shapeshifter-state: /srv/leap/shapeshifter-state
        - /srv/leap/dns-cache: /var/cache/knot-resolver
        - /etc/openvpn: /etc/openvpn
        - /dev/net: /dev/net
        - /etc/knot-resolver: /etc/knot-resolver
        - /var/lib/GeoIP: /var/lib/GeoIP/
      env:
        # Shapeshifter specific environment variables
        LHOST: "{{ gateway_address | default(ip) }}"
        RHOST: "{{ gateway_address | default(ip) }}"
        RPORT: '1194'
        OBFSPORT: '23042'
        EXTORPORT: '3334'
        _CHAP_OPTIONS: --no-syslog
        LOGLEVEL: DEBUG

vpnweb:
  scheduling_group: backend
  num_instances: 1
  monitoring_endpoints:
    - job_name: vpnweb
      port: 8001
      scheme: http
  containers:
    - name: vpnweb
      image: registry.0xacab.org/leap/vpnweb:latest
      port: 8000
      volumes:
        - /etc/leap: /etc/leap
      env:
        VPNWEB_API_PATH: /etc/leap/config/vpnweb
        VPNWEB_CACRT: /etc/leap/ca/client_ca.crt
        VPNWEB_CAKEY: /etc/leap/keys/client_ca.key
        VPNWEB_PROVIDER_CA: /etc/leap/ca/api_ca.crt
        VPNWEB_AUTH: "{{ vpnweb_auth }}"
        VPNWEB_SIP_USER: "{{ vpnweb_sip_user }}"
        VPNWEB_SIP_PASS: "{{ vpnweb_sip_password }}"
        VPNWEB_AUTH_SECRET: "{{ vpnweb_auth_secret }}"
        VPNWEB_SIP_HOST: "{{ vpnweb_sip_host }}"
        VPNWEB_SIP_PORT: "{{ vpnweb_sip_port }}"
        VPNWEB_SIP_LIBR_LOCATION: "{{ vpnweb_sip_location }}"
        VPNWEB_SIP_TERMINATOR: "{{ vpnweb_sip_terminator }}"
        VPNWEB_PASSWORD_POLICY: "{{ vpnweb_password_policy }}"
  public_endpoints:
    - name: api
      port: 8000
      scheme: http
