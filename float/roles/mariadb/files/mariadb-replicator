#!/bin/bash
#
# Manage replication state for a MariaDB (>=10) instance.
#
# Uses a state file, stored in the data directory, to detect state
# changes. The possible states are 'init' (empty database on first
# install), 'slave' and 'master'. The state file stores a fingerprint
# of the master connection parameters, to detect changes in those as
# well.
#
# The script can manage one or more databases, passed as arguments
# on the command line, or all the databases if no arguments are given.
#
# When first setting up a slave, it will pull a dump of the current
# contents from the master, using the credentials of the replication
# user: make sure that it also has SELECT permissions on all tables.
#

set -o pipefail

usage() {
    cat <<EOF
Usage: $0 [<OPTIONS>] [<DATABASE>...]
Options:
  --master | --slave
  --defaults-file FILE
  --master-host HOST
  --master-port PORT
  --replication-user USER
  --replication-password PASSWORD
  
EOF
    exit 2
}

die() {
    echo "ERROR: $*" >&2
    exit 1
}

get_current_state() {
    if [ -e "${DATADIR}/.replication_state" ]; then
        cat "${DATADIR}/.replication_state"
    else
        echo "init"
    fi
}

compute_new_state() {
    new_master_hash=$(echo -n "${master_host}:${master_port}:${replication_user}:${replication_password}" | sha1sum | cut -d' ' -f 1)
    echo "${role}:${new_master_hash}"
}

pull_database_from_master() {
    MYSQLDUMP="mysqldump --host=${master_host} --port=${master_port} --user=${replication_user} --password=${replication_password} --gtid --master-data --opt --single-transaction --quick"
    if [ -z "${databases}" ]; then
        ${MYSQLDUMP} --all-databases | ${MYSQL}
    else
        ${MYSQLDUMP} --databases ${databases} | ${MYSQL}
    fi
}

setup_slave() {
    ${MYSQL} -NBe "CHANGE MASTER TO
MASTER_HOST='${master_host}',
MASTER_PORT=${master_port},
MASTER_USER='${replication_user}',
MASTER_PASSWORD='${replication_password}',
MASTER_CONNECT_RETRY=10,
MASTER_USE_GTID=slave_pos;
START SLAVE"
}

promote_slave_to_master() {
    ${MYSQL} -NBe "STOP SLAVE; RESET MASTER"
}

demote_master_to_slave() {
    ${MYSQL} -NBe "RESET MASTER"
    setup_slave
}

change_slave_parameters() {
    ${MYSQL} -NBe "STOP SLAVE"
    setup_slave
}

# Parse command-line options.
defaults_file=/etc/mysql/my.cnf
master_host=
master_port=3306
replication_user=
replication_password=
role=
databases=

while [ $# -gt 0 ]; do
    case "$1" in
        --defaults-file=*)
            defaults_file="${1##--defaults-file=}"
            ;;
        --defaults-file)
            defaults_file="$2"
            shift
            ;;

        --master-host=*)
            master_host="${1##--master-host=}"
            ;;
        --master-host)
            master_host="$2"
            shift
            ;;

        --master-port=*)
            master_port="${1##--master-port=}"
            ;;
        --master-port)
            master_port="$2"
            shift
            ;;

        --replication-user=*)
            replication_user="${1##--replication-user=}"
            ;;
        --replication-user)
            replication_user="$2"
            shift
            ;;

        --replication-password=*)
            replication_password="${1##--replication-password=}"
            ;;
        --replication-password)
            replication_password="$2"
            shift
            ;;

        --master|--slave)
            if [ -n "${role}" ]; then
                echo "Must specify only one of --master or --slave" >&2
                echo "Run with --help for help." >&2
                exit 2
            fi
            role="${1##--}"
            ;;

        -h|--help)
            usage
            ;;

        -*)
            echo "Unknown argument: $1" >&2
            echo "Run with --help for help." >&2
            exit 2
            ;;

        *)
            databases="${databases} $1"
            ;;
    esac
    shift
done

if [ -z "${role}" ]; then
    echo "Must specify the desired role using either --master or --slave" >&2
    exit 2
fi

if [ "${role}" = "slave" ]; then
    if [ -z "${master_host}" -o -z "${master_port}" ]; then
        echo "Must specify --master-host and --master-port" >&2
        exit 2
    fi
    if [ -z "${replication_user}" -o -z "${replication_password}" ]; then
        echo "Must specify --replication-user and --replication-password" >&2
        exit 2
    fi
fi

MYSQL=/usr/bin/mysql
if [ -e "${defaults_file}" ]; then
    MYSQL="${MYSQL} --defaults-file=${defaults_file}"
fi

DATADIR=$(${MYSQL} -NBe 'select @@datadir;')
[ -n "${DATADIR}" ] || die "can't connect to mysql"
[ -d "${DATADIR}" ] || die "data directory ${DATADIR} does not exist"

# Compare current and desired state.
cur_state=$(get_current_state)
new_state=$(compute_new_state)
if [ "${cur_state}" = "${new_state}" ]; then
    echo "Nothing to do."
    exit 0
fi

# Handle the transition.
cur_role="${cur_state%%:*}"

echo "replication state needs to change: ${cur_role} -> ${role}"

case "${cur_role}->${role}" in
    "init->master")
        # Nothing to do in this case.
        ;;
    "init->slave")
        # Pull the database contents from the master to initialize a slave.
        pull_database_from_master
        setup_slave
        ;;
    "slave->slave")
        # Master parameters have changed.
        change_slave_parameters
        ;;
    "slave->master")
        promote_slave_to_master
        ;;
    "master->slave")
        demote_master_to_slave
        ;;
    *)
        die "unsupported state transition ${cur_role} -> ${role}"
        ;;
esac

if [ $? -gt 0 ]; then
    die "could not complete state transition"
fi

# Update the state file.
echo "${new_state}" > "${DATADIR}/.replication_state"

exit 0
